'use strict';

var S         = require('string')
var delimit   = require('delimit')
var monitor   = {}
var fs        = require('fs')
var path      = require('path')
var packPath  = path.join(process.cwd(), 'package.json')
packPath      = fs.existsSync(packPath) ? packPath : './package.json'
var pack      = require(packPath)
var realm     = process.env.APPNAME || pack.name
var path      = require('path')
var TEST      = process.env.TEST

function packageInfo(name) {
  var paths = module.paths
  for (var i=0; i < paths.length; i++) {
    var filepath = path.join(paths[i], name, 'package.json')
    if (fs.existsSync(filepath)) {
      return require(filepath)
    }
  }
  return { name: name, version: '?' }
}



function thisAppPackage() {
  var packpath = path.dirname(require.main.filename)
  var filepath = path.join(packpath, 'package.json')
  if (fs.existsSync(filepath)) return require(filepath)
  packpath     = path.dirname(packpath)
  filepath     = path.join(packpath, 'package.json')
  return require(filepath) // check at most one level up
}



var TRACE_KEY = process.env.TRACE_API_KEY // error: [trace] Make sure to require Trace before New Relic, otherwise Trace won't start
if (TRACE_KEY) {
  var apack = packageInfo('@risingstack/trace')
  console.log(delimit(S(apack.name).humanize().s), apack.version)
  var trace = require('@risingstack/trace')
  monitor[apack.name] = apack.version
}



var envic       = require('envic')
var NEW_RELIC   = envic('NEW_RELIC') || {}
NEW_RELIC.agent = NEW_RELIC.agent || process.env.NEW_RELIC_LICENSE_KEY
if (NEW_RELIC.agent) {
  var js = [] // from node_modules/newrelic/newrelic.js
  var apack = packageInfo('newrelic')
  console.log(delimit(S(apack.name).humanize().s), apack.version, NEW_RELIC.agent)
  js.push("'use strict'")
  js.push("exports.config = {")
  js.push("app_name: ['" + realm + "'],")
  js.push("license_key: '" + NEW_RELIC.agent + "',")
  js.push("logging: {level: '" + (NEW_RELIC.log_level || 'info') + "'}}")
  var found = false;
  var ok = false;
  var filePath = path.join(require.resolve('newrelic').replace('index.js', ''), 'newrelic.js')
  if (process.env.NEW_RELIC_CONFIG_FILE) {
    try {
      fs.writeFileSync(process.env.NEW_RELIC_CONFIG_FILE, js.join("\n"), 'utf8')
      console.log(delimit(S(apack.name).humanize().s), process.env.NEW_RELIC_CONFIG_FILE)
      found = true;
    } catch(e) {
      console.error(delimit(S(apack.name).humanize().s + ' Error'), e)
    }
  }
  if (!found && fs.existsSync(filePath)) {
    try {
      fs.writeFileSync(filePath, js.join("\n"), 'utf8')
      console.log(delimit(S(apack.name).humanize().s), filePath)
      if (process.env.NEW_RELIC_CONFIG_FILE) {
        fs.writeFileSync(process.env.NEW_RELIC_CONFIG_FILE, js.join("\n"), 'utf8')
        console.log(delimit(S(apack.name).humanize().s), process.env.NEW_RELIC_CONFIG_FILE)
      }
      found = true;
    } catch(e) {
      console.error(delimit(S(apack.name).humanize().s + ' Error'), e)
    }
  }
  try {
    if (found) {
      require('newrelic')
      ok = true;
      monitor[apack.name] = apack.version
    }
  } catch (e) {
    console.error(delimit(S(apack.name).humanize().s + ' Error'), e)
  }
  if (!found) {
    console.error(delimit(S(apack.name).humanize().s + ' Error'), 'newrelic.js not found', filePath,  process.env.NEW_RELIC_CONFIG_FILE)
  }
} // endif (NEW_RELIC.agent) {



var ROLLBAR = process.env.ROLLBAR_ACCESS_TOKEN
var ROLLBAR_ENDPOINT = process.env.ROLLBAR_ENDPOINT
if (ROLLBAR && ROLLBAR_ENDPOINT) {
  var apack = packageInfo('rollbar')
  console.log(delimit(S(apack.name).humanize().s), apack.version, ROLLBAR_ENDPOINT)
  if (!TEST) {
    var rollbar = require("rollbar")
    rollbar.init(ROLLBAR, {
      environment: "heroku", // TODO: make environment variable
      endpoint: ROLLBAR_ENDPOINT
    });
  }
  console.log(delimit(S(apack.name).humanize().s), 'initialized')
  if (!TEST) {
    rollbar.handleUncaughtExceptions(ROLLBAR, {exitOnUncaughtException: false})
  }
  console.log(delimit(S(apack.name).humanize().s), 'handleUncaughtExceptions')
  var filepath = path.join(path.dirname(module.filename), 'package.json')
  var pack = thisAppPackage()
  console.log(delimit(S(apack.name).humanize().s), 'reportMessage', pack.name, pack.version) // TODO: for some reason rollbar isn't working with Heroku
  if (!TEST) {
    rollbar.reportMessage(pack.name + '/' + pack.version + ' started');
  }
  monitor[apack.name] = apack.version
}



var AIRBRAKE = process.env.AIRBRAKE_API_KEY
var AIRBRAKE_PROJECT_ID = process.env.AIRBRAKE_PROJECT_ID
if (AIRBRAKE && AIRBRAKE_PROJECT_ID) {
  var apack = packageInfo('airbrake')
  console.log(delimit(S(apack.name).humanize().s), apack.version, AIRBRAKE_PROJECT_ID) // TODO: for some reason airbrake isn't working with heroku. papertrail interferring somehow?
  var airbrake = require('airbrake').createClient(
    AIRBRAKE_PROJECT_ID, // Project ID
    AIRBRAKE // Project key
  );
  airbrake.handleExceptions();
  monitor[apack.name] = apack.version
}



module.exports = monitor
