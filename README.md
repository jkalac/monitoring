# monitoring

Auto monitoring package for @risingstack/trace, newrelic, rollbar and airbrake.



## Usage

```js
var active = require('monitoring')
// active: active packages with versions
// {
//   "@risingstack/trace": "3.0.2",
//   "newrelic": "1.36.0",
//   "rollbar": "0.6.3",
//   "airbrake": "1.2.0",
// }
```

## Environment variable flags

- `TRACE_API_KEY` @risingstack/trace
- `NEW_RELIC_LICENSE_KEY` newrelic and `NEW_RELIC` as jsonic. Ex: `agent:LicenseKey,log_level:debug`
- `ROLLBAR_ACCESS_TOKEN` and `ROLLBAR_ENDPOINT` rollbar
- `AIRBRAKE_API_KEY` and `AIRBRAKE_PROJECT_ID` airbrake // TODO: debug
- `LOADERIO_API_KEY` and `LOADERIO_TEST` loader.io // TODO: to be added, used with ExpressJS

## License

Private
